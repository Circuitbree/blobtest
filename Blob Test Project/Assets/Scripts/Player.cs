﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public GameObject playerObject;
    public GameObject bullets;

    public Transform bulletSpawn;

    public float moveSpeed = 20f;

    public Vector3 direction;

	// Use this for initialization
	void Start () {
        playerObject = GameObject.FindWithTag("Player");
        bulletSpawn = GameObject.FindWithTag("blt").transform;
        Quaternion rotation = playerObject.GetComponent<Transform>().localRotation;
        direction = new Vector3(rotation.x, rotation.y, rotation.z);

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.A))
        {
            playerObject.GetComponent<Transform>().Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            playerObject.GetComponent<Transform>().Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.W))
        {
            playerObject.GetComponent<Transform>().Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            playerObject.GetComponent<Transform>().Translate(Vector3.back * moveSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            shoot();
        }

    }

    void shoot()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bullets,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }


}
